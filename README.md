Prerequisites
-------------

1. Make sure python3 is installed locally

2. create python env:
    ```
    export WORKON_HOME=~/Envs
    mkdir -p $WORKON_HOME
    source /usr/local/bin/virtualenvwrapper.sh
    mkvirtualenv --python=$(which python3.6) aws_terraform_ansible
    ```

    You can add an alias to ~/.bash_aliases to switch to this env:

      `alias ataenv='export WORKON_HOME=~/Envs && source /usr/local/bin/virtualenvwrapper.sh && workon aws_terraform_ansible'`

3. Install dependencies:
    `$ pip install -r requirements.txt`

4.  Run playbook to install terraform locally (your local user should be in sudoers):
    `ansible/terraform_install.yml`

    The current version of terraform can be checked with the following command:
      `$ terraform --version`

5. Prepare local environment to work with AWS:

  - install awscli

  - create /home/<your_user>/.aws/credentials file with the Key Pair and [terraform] as profile name.
      ```
      [terraform]
      region = us-east1
      aws_access_key_id = <access_key>
      aws_secret_access_key = <>secret_key
      ```

  - You will need to add ssh key to allow ssh connection to EC2 (it is also required by ansible):
    ssh-add <path_to_private_key>


Deployment
----------
Install terraform requirements:
`terraform init` (in the terraform directory)

Run the following playbook to create the infrastructure:
`ansible/deploy_infrastructure.yml`

Run the following playbook to remove the infrastructure:
`ansible/destroy_infrastructure.yml`
