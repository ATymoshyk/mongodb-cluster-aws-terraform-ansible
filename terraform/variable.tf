### variable.tf
variable "aws_region" {
  description = "AWS region on which we will run instances"
  default = "us-east-1"
}
variable "ami" {
  description = "CentOS AMI"
  default = "ami-7d579d00"
  # default = "ami-0987654" - this was given in the task
}
variable "instance_type" {
  description = "Instance type"
  default = "t2.micro"
}
variable "key_name" {
  description = "Desired name of Keypair..."
  default = "atym_testkey"
}
variable "bootstrap_path" {
  description = "Script to install Docker Engine"
  default = "install_docker_machine_compose.sh"
}
variable "az1" {
  default = "us-east-1a"
}
variable "az2" {
  default = "us-east-1b"
}
variable "az3" {
  default = "us-east-1c"
}
variable "num_instances" {
  default = "1"
}
variable "volume_size" {
  default = "1" # To be changed to 1000 according to the task
}
