output "private_ip" {
   value = "${formatlist("%v", aws_instance.mongodb.*.private_ip)}"
}

output "public_ip" {
   value = "${formatlist("%v", aws_instance.mongodb.*.public_ip)}"
}
