variable "credentials_file" {
  default = "~/.aws/credentials"
}

provider "aws" {
  region                  = "us-east-1"
  shared_credentials_file = "${var.credentials_file}"
  profile                 = "terraform"
}

resource "aws_security_group" "allow_all" {
  name        = "allow_all"
  description = "Allow all inbound traffic"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "allow_all"
  }
}

resource "aws_security_group_rule" "allow_all" {
  type            = "egress"
  from_port       = 0
  to_port         = 65535
  protocol        = "tcp"
  cidr_blocks     = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.allow_all.id}"
}

resource "aws_instance" "mongodb" {
  connection {
    user = "centos"
  }
  availability_zone = "${var.az1}"
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  # subnet_id = "subnet-123"
  ebs_block_device {
    device_name = "/dev/sdb"
    volume_size = "${var.volume_size}"
    volume_type = "gp2"
    delete_on_termination = true
  }
  security_groups = [ "${aws_security_group.allow_all.name}" ]
  tags {
    Name = "mongo1"
  }
}

resource "aws_instance" "mongodb2" {
  connection {
    user = "centos"
  }
  availability_zone = "${var.az2}"
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  # subnet_id = "subnet-456"
  ebs_block_device {
    device_name = "/dev/sdb"
    volume_size = "${var.volume_size}"
    volume_type = "gp2"
    delete_on_termination = true
  }
  security_groups = [ "${aws_security_group.allow_all.name}" ]
  tags {
    Name = "mongo2"
  }
}
